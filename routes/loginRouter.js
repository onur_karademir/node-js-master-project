const express = require("express");
const router = express.Router();
const User = require("../models/userModel");


router.get("/login",(req,res)=> {
    res.render("user/login")
});
router.post("/login",(req,res)=> {
    const {email,password} = req.body;

    User.findOne({email:email}).then(user=> {
        if (user) {
            if(user.password == password) {
                   ///session init and 
                   req.session.userNameLogin = user.nick
                   req.session.flashMessage = {
                    content: "giriş başarılı ana sayfaya yönlendirildiniz....",
                  };
                   res.redirect("/")
            }else{
                req.session.flashMessage = {
                    content: "hatalı giriş yaptınız...",
                  };
                res.redirect("/user/login")
            }
        }
    })
});



module.exports = router;