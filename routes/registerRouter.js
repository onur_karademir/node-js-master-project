const express = require("express");
const router = express.Router();
const User = require("../models/userModel");
router.get("/register", (req, res) => {
  res.render("user/register");
});
router.post("/register", (req, res) => {
  const { nick, email, password,passwordtwo } = req.body;

  if (!nick || !email || !password) {
    req.session.flashMessage = {
      content: "Tüm alanları doldurmalısınız",
    };
    res.redirect("/user/register");
  }
  if (password != passwordtwo) {
    req.session.flashMessage = {
      content: "Parola uyuşmadı...",
    };
    res.redirect("/user/register");
  }
  if ((password.length < 6) || (passwordtwo.length <6)) {
    req.session.flashMessage = {
      content: "Parola en az altı karakter veya sayıdan oluşmalı",
    };
    res.redirect("/user/register");
  } else {
      User.findOne({email:email}).then(user=> {
          if (user) {
            req.session.flashMessage = {
                content: "bu e-mail kullnılıyor",
            };
            res.redirect("/user/register");
          }else {
            User.findOne({nick:nick}).then(user=> {
              if (user) {
                req.session.flashMessage = {
                  content: "bu nick kullnılıyor",
              };
              res.redirect("/user/register");
              }
              else {
                var user = new User({
                    nick: nick,
                    email: email,
                    password: password,
                  });
                  user.save().then((result) => {
                      console.log(result);
                      req.session.flashMessage = {
                        content: "kayıt başarılı giriş yapabilirsiniz...",
                    };
                      res.redirect("/user/login");
                    })
                    .catch((err) => {
                      console.log(err);
                    });
              }
            })
          }
      })
  }
});

module.exports = router;
