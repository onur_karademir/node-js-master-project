//library call down here
const express = require("express");
const app = express();
const expressLayouts = require("express-ejs-layouts");
const flash = require("connect-flash");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const cookieParser = require("cookie-parser");
const fileUpload = require("express-fileupload");
const session = require("express-session");
const passport = require("passport");
require("./config/passport")(passport);

//router call down here
const indexRouter = require("./routes/indexRouter");
const registerRouter = require("./routes/registerRouter");
const loginRouter = require("./routes/loginRouter");
const logoutRouter = require("./routes/logoutRouter");

//mongoose connect here
mongoose
  .connect(
    "mongodb+srv://aidenpearce:Onur050263007@cluster0.bvq9p.mongodb.net/node-master-db?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then((db) => console.log("db is connected database..."))
  .catch((error) => console.log(error));

//app set down here
app.set("view engine", "ejs");
//app use down here
app.use(expressLayouts);
app.use(cookieParser());
app.use("/public", express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());
app.use(flash());
app.use(session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use("/", (req, res, next) => {
  res.locals.loggedIn = req.isAuthenticated();
  next();
});
app.use("/", (req, res, next) => {
  res.locals.userName = { user: req.user };
  next();
});
app.use((req, res, next) => {
  res.locals.flashMessage = req.session.flashMessage;
  delete req.session.flashMessage;
  next();
});
app.use((req, res, next) => {
  res.locals.userNameLogin = req.session.userNameLogin;
  next();
});

app.use("/", indexRouter);
app.use("/user", registerRouter);
app.use("/user", loginRouter);
app.use("/user", logoutRouter);

//app listen port 5000
app.listen(process.env.PORT || 5000, () => {
  console.log("local host://5000 ===>>> app start...");
});
