const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var UserSchema = new Schema({
  nick: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
  },
  password: {
    type: String,
    required: true,
    trim: true,
  }
});


var User = mongoose.model("User",UserSchema);
module.exports = User